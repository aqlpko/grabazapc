#include "vertexdata.h"

VertexData::VertexData(int n_)
{
    n=n_;
    verticesNumber=new GLsizeiptr[n];
    indicesNumber=new GLuint[n];
    verticesPNumber=new GLsizeiptr[n];
    vertices=new GLfloat*[n];
    indices=new GLushort*[n];
    verticesP=new GLfloat*[n];
#include "zasoby\indexdata0.h"
#include "zasoby\vertexdata0.h"
#include "zasoby\indexdata1.h"
#include "zasoby\vertexdata1.h"
}

VertexData::~VertexData(){}

GLfloat** VertexData::getVertices()
{
    return vertices;
}
GLushort** VertexData::getIndices()
{
    return indices;
}
GLuint* VertexData::getIndicesNumber()
{
    return indicesNumber;
}
GLsizeiptr* VertexData::getVerticesNumber()
{
    return verticesNumber;
}
GLfloat** VertexData::getVerticesP()
{
    return verticesP;
}
GLsizeiptr* VertexData::getVerticesPNumber()
{
    return verticesPNumber;
}
