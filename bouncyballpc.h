#ifndef BOUNCYBALLPC_H
#define BOUNCYBALLPC_H

#include <QTimer>

class BouncyBallPc:public QObject
{
    Q_OBJECT
    int counter;
    QTimer update;
public:
    BouncyBallPc(float dt);
    ~BouncyBallPc();
    void start();
    void setGraphicsObject();
public slots:
    void step();
signals:
    void newFrame();
    void addObject(int &id,bool isPhysical,int modelN,float* pos,float angle,float* v=NULL,float omega=0.0f,float mass=0.0f,float friction=0.0f,float restutition=0.0f,bool isDynamic=true,float density=0.0f);
    void setMemory(int n);

};

#endif // BOUNCYBALLPC_H
