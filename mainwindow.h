#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVector2D>
#include <QMouseEvent>
#include "physicsengine.h"
#include <QPushButton>
#include "vertexdata.h"
#include "bouncyballpc.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void mousePressEvent(QMouseEvent *event);
    void setForce(int id, int modelN, float *force);
    float* getPos(int id, int modelN);
    int getId(int modelN);

public slots:
    void updateGame();
    void setGraphicsObject();
    void addObject(int &id,bool isPhysical, int modelN, float* pos, float angle, float* v=NULL, float omega=0.0f, float mass=0.0f, float friction=0.0f, float restutition=0.0f, bool isDynamic=true, float density=0.0f);
    void setMemory(int n);

private:
    Ui::MainWindow *ui;
    float dt;
    int screenDimension[2];
    float sceneDimension[7];
    float fingerTranslate[2];
    int modelNumber, *objectNumber, *physicalObject, *graphicsObject;
    QMatrix4x4 ***viewMatrix;
    float windRotate,windScale;
    PhysicsEngine *physics;
    VertexData *data;
    int **id;
    BouncyBallPc *game;
};


#endif // MAINWINDOW_H
