#ifndef VERTEXDATA_H
#define VERTEXDATA_H

#include <QtOpenGL/QGLWidget>

class VertexData
{
    GLfloat **vertices;
    GLushort **indices;
    GLfloat **verticesP;
    int n;
    GLuint *indicesNumber;
    GLsizeiptr *verticesNumber;
    GLsizeiptr *verticesPNumber;
public:
    VertexData(int n_);
    ~VertexData();
    GLfloat **getVertices();
    GLushort **getIndices();
    GLfloat **getVerticesP();
    GLuint *getIndicesNumber();
    GLsizeiptr *getVerticesNumber();
    GLsizeiptr *getVerticesPNumber();
};

#endif // VERTEXDATA_H
