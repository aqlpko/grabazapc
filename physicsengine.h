#ifndef PHYSICSENGINE_H
#define PHYSICSENGINE_H

#include "Box2D/Box2D.h"
#include <QList>
#include <QMatrix4x4>

class PhysicsEngine
{
    b2World *world;
    b2Vec2 gravity;

    b2BodyDef groundBodyDef;
    b2Body *groundBody;
    b2PolygonShape groundBox;

    float32 dt;
    int32 velocityIterations;
    int32 positionIterations;

    float gameSpeed;

    int modelNumber;
    int* objectNumber;

    struct object
    {
        b2BodyDef *bodyDef;
        b2FixtureDef *fixtureDef;
        b2Body *body;
        int id;
    };

    QList<object*> *list;

public:
    PhysicsEngine(float dt_, int modelNumber_, float *gravity_, float* dimension);
    ~PhysicsEngine();
    void createWorld(float* dimension);
    void updatePhysics(QMatrix4x4*** viewMatrix, int *objectNumber, int *graphicsObject);
    void createObject(int modelN,float *data1,unsigned short* data2,int nIndices, float* pos, float angle, float* v, float omega, float mass, float friction, float restutition, bool isDynamic, float density,int id_);
    void setForce(int id, int modelN,float *force_);
    float* getPos(int id, int modelN);
};

#endif // PHYSICSENGINE_H
