#include "physicsengine.h"

PhysicsEngine::PhysicsEngine(float dt_, int modelNumber_, float *gravity_, float *dimension)
{
    dt=dt_;
    modelNumber=modelNumber_;
    objectNumber=new int[modelNumber];
    list=new QList<object*>[modelNumber];
    gravity=b2Vec2(gravity_[0],gravity_[1]);
    createWorld(dimension);
}
PhysicsEngine::~PhysicsEngine()
{

}

void PhysicsEngine::createWorld(float *dimension)
{
    gameSpeed=1.0f;

    world=new b2World(gravity);

    groundBodyDef.position.Set(0.0f,dimension[1]*-1.0f-0.1f);
    groundBody=world->CreateBody(&groundBodyDef);
    groundBox.SetAsBox(dimension[0],0.1f);
    groundBody->CreateFixture(&groundBox,0.0f);

    groundBodyDef.position.Set(dimension[0]*-1.0f-0.1f,0.0f);
    groundBody=world->CreateBody(&groundBodyDef);
    groundBox.SetAsBox(0.1f,dimension[1]);
    groundBody->CreateFixture(&groundBox,0.0f);

    groundBodyDef.position.Set(dimension[0]+0.1f,0.0f);
    groundBody=world->CreateBody(&groundBodyDef);
    groundBox.SetAsBox(0.1f,dimension[1]);
    groundBody->CreateFixture(&groundBox,0.0f);

    groundBodyDef.position.Set(0.0f,dimension[1]+0.1f);
    groundBody=world->CreateBody(&groundBodyDef);
    groundBox.SetAsBox(dimension[0],0.1f);
    groundBody->CreateFixture(&groundBox,0.0f);

    dt*=gameSpeed;
    velocityIterations=8;
    positionIterations=3;
}
void PhysicsEngine::createObject(int modelN, float *data1, unsigned short* data2, int nIndices, float* pos, float angle, float* v, float omega, float mass, float friction, float restutition, bool isDynamic, float density, int id_)
{
    object* newObject=new object;
    newObject->id=id_;
    newObject->bodyDef=new b2BodyDef;
    if(isDynamic)
        newObject->bodyDef->type=b2_dynamicBody;
    else
        newObject->bodyDef->type=b2_staticBody;
    newObject->bodyDef->position.Set(pos[0],pos[1]);
    newObject->bodyDef->angle=angle*0.01744f;
    newObject->body=world->CreateBody(newObject->bodyDef);

    b2Vec2 vertices[3];

    for(int j=0;j<nIndices;j+=3)
    {
        for(int i=0;i<3;i++)
            vertices[i].Set(data1[data2[j+i]*4],data1[(data2[j+i]*4)+1]);

        b2PolygonShape shape;
        shape.Set(vertices,3);
        newObject->fixtureDef=new b2FixtureDef;
        newObject->fixtureDef->shape=&shape;
        newObject->fixtureDef->friction=friction;
        newObject->fixtureDef->restitution=restutition;
        newObject->fixtureDef->density=density;
        newObject->body->CreateFixture(newObject->fixtureDef);
    }

    newObject->body->SetLinearVelocity(b2Vec2(v[0],v[1]));
    newObject->body->SetAngularVelocity(omega);
    list[modelN].append(newObject);
}
void PhysicsEngine::updatePhysics(QMatrix4x4 ***viewMatrix, int* objectNumber, int* graphicsObject)
{
    b2Vec2 pos;
    world->Step(dt,velocityIterations,positionIterations);
    for(int i=0;i<modelNumber;i++)
        for(int j=graphicsObject[i];j<objectNumber[i];j++)
        {
            pos=list[i][j]->body->GetPosition();
            float x=pos.x;
            float y=pos.y;
            viewMatrix[i][j]->setToIdentity();
            viewMatrix[i][j]->translate(pos.x,pos.y);
            viewMatrix[i][j]->rotate(list[i][j]->body->GetAngle()*57.32f,0,0,1);
        }
}
void PhysicsEngine::setForce(int id, int modelN,float *force_)
{
    int n=list[modelN].size();
    for(int i=0;i<n;i++)
    {
        if(list[modelN][i]->id==id)
        {
            b2Vec2 force;
            force.Set(force_[0],force_[1]);
            break;
        }
    }

//    b2Vec2 force=list[modelN][objectN]->body->GetPosition();
//    force.x-=pos[0];
//    force.y-=pos[1];
//    force.x=factor*force.x;
//    force.y=factor*force.y;
//    list[modelN][objectN]->body->ApplyForceToCenter(force,true);
}

float* PhysicsEngine::getPos(int id, int modelN)
{
    int n=list[modelN].size();
    for(int i=0;i<n;i++)
    {
        if(list[modelN][i]->id==id)
        {
            b2Vec2 pos=list[modelN][i]->body->GetPosition();
            float* pos_=new float[2];
            pos_[0]=pos.x;
            pos_[1]=pos.y;
            return pos_;
        }
    }
}
