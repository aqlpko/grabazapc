#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTime>
#include <QMatrix4x4>
#include <cmath>
#include <QtOpenGL/QGLWidget>
#include <QGuiApplication>
#include <QScreen>
#include <QSize>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    float gravity[2];

    gravity[0]=0.0f;
    gravity[1]=-9.81f;
    screenDimension[0]=480;
    screenDimension[1]=640;
    int fps=40;
    modelNumber=2;
    sceneDimension[0]=-0.5;//left,right,bottom,top,near,far,angle
    sceneDimension[1]=0.5;

    float aspect=(float)screenDimension[0]/screenDimension[1];
    sceneDimension[2]=sceneDimension[0]/aspect;
    sceneDimension[3]=sceneDimension[1]/aspect;
    sceneDimension[4]=3;
    sceneDimension[5]=7;
    sceneDimension[6]=0;
    objectNumber=new int[modelNumber];
    physicalObject=new int[modelNumber];
    graphicsObject=new int[modelNumber];
    id=new int*[modelNumber];

    for(int i=0;i<modelNumber;i++)
    {
        objectNumber[i]=0;
        physicalObject[i]=0;
        graphicsObject[i]=0;
    }

    viewMatrix=new QMatrix4x4**[modelNumber];
    dt=1.0/fps;

    float dimension[2];
    dimension[0]=sceneDimension[1];
    dimension[1]=sceneDimension[3];
    physics=new PhysicsEngine(dt,modelNumber,gravity,dimension);

    data=new VertexData(modelNumber);
    ui->render->initializeValues(screenDimension,sceneDimension,modelNumber,data->getVertices(),data->getIndices(),data->getIndicesNumber());

    game=new BouncyBallPc(dt);
    connect(game,SIGNAL(newFrame()),this,SLOT(updateGame()));
    connect(game,SIGNAL(setMemory(int)),this,SLOT(setMemory(int)));
    connect(game,SIGNAL(addObject(int&,bool,int,float*,float,float*,float,float,float,float,bool,float)),this,SLOT(addObject(int&,bool,int,float*,float,float*,float,float,float,float,bool,float)));
    game->start();
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::updateGame()
{
    physics->updatePhysics(viewMatrix,objectNumber,graphicsObject);
    setGraphicsObject();
    ui->render->updateGraphics(viewMatrix,objectNumber);
}
void MainWindow::setGraphicsObject()
{

}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    fingerTranslate[0]=(((float)event->x())/screenDimension[0])*sceneDimension[1]*2-sceneDimension[1];
    fingerTranslate[1]=(((float)screenDimension[1]-event->y())/screenDimension[1])*sceneDimension[3]*2-sceneDimension[3];
}

void MainWindow::addObject(int &id, bool isPhysical, int modelN, float* pos, float angle, float* v, float omega, float mass, float friction, float restutition, bool isDynamic, float density)
{

    int newId=getId(modelN);
    id=newId;
    objectNumber[modelN]++;

    if(isPhysical)
    {
        float* d1=data->getVertices()[modelN];
        GLushort* d2=data->getIndices()[modelN];
        int n=data->getIndicesNumber()[modelN];
        physics->createObject(modelN,d1,d2,n,pos,angle,v,omega,mass,friction,restutition,isDynamic,density,newId);
        physicalObject[modelN]++;
    }
    else
    {

        graphicsObject[modelN]++;
        int n=graphicsObject[modelN]-1;
        viewMatrix[modelN][n]->setToIdentity();
        viewMatrix[modelN][n]->translate(pos[0],pos[1]);
        viewMatrix[modelN][n]->rotate(angle,0,0,1);
    }

}

void MainWindow::setForce(int id, int modelN, float *force)
{
    physics->setForce(id,modelN,force);
}

float* MainWindow::getPos(int id, int modelN)
{
    return physics->getPos(id,modelN);
}

int MainWindow::getId(int modelN)
{
    for(int i=0;i<10;i++)
    {
        if(id[modelN][i]==0)
        {
            id[modelN][i]=i+1;
            break;
        }
    }
}

void MainWindow::setMemory(int n)
{
    for(int i=0;i<modelNumber;i++)
    {
        id[i]=new int[n];
        viewMatrix[i]=new QMatrix4x4*[n];
        for(int j=0;j<n;j++)
        {
            id[i][j]=0;
            viewMatrix[i][j]=new QMatrix4x4;
        }
    }
}
