#include "bouncyballpc.h"

BouncyBallPc::BouncyBallPc(float dt)
{

    update.setSingleShot(false);
    update.setInterval(dt*1000);
    connect(&update,SIGNAL(timeout()),this,SLOT(step()));
}

BouncyBallPc::~BouncyBallPc()
{

}

void BouncyBallPc::start()
{

        emit setMemory(10);
        int id;
        float pos[2];
        pos[0]=0.0f;
        pos[1]=0.6f;
        emit addObject(id,false,0,pos,0.0f);
        pos[0]=0.0f;
        pos[1]=0.0f;
        float v[2];
        v[0]=-2.8f;
        v[1]=10.0f;
        emit addObject(id,true,1,pos,0.0f,v,0.0f,0.45f,0.2f,0.7f,true,8.88f);

        update.start();

}

void BouncyBallPc::step()
{

    emit newFrame();
}
